import React, {useState, useEffect} from "react";
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBDropdown,
MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem, MDBIcon } from "mdbreact";
import { BrowserRouter as Router } from 'react-router-dom';
import {NavLink} from 'reactstrap'
import './navbar.css';

const Navbar = ()=> {

const [isOpen, setIsOpen] = useState(false);
const [user, setUser] = useState({})

useEffect(()=>{
  if (sessionStorage.token){
    let user = JSON.parse(sessionStorage.user)
    setUser(user);
  }else{
    console.log(user);
  }
}, [])

const toggleCollapse = () => {
  setIsOpen(!isOpen);
}

const handleLogout = () =>{
  sessionStorage.clear();
  window.location.replace('#/')
}
  return (
    <Router>
      <MDBNavbar className="navbar"  dark expand="md">
        <MDBNavbarBrand>
          <strong className="holo"><span class="holo-v">HO</span>lo</strong>
        </MDBNavbarBrand>
        <MDBNavbarToggler onClick={toggleCollapse} />
        <MDBCollapse id="navbarCollapse3" isOpen={!isOpen} navbar>
          <MDBNavbarNav left>
          <MDBNavItem>
              <NavLink href="#/">Home</NavLink>
            </MDBNavItem>
            <MDBNavItem>
              <NavLink href="#/book">Book Now!</NavLink>
            </MDBNavItem>
          {sessionStorage.token ?
            <MDBNavItem>
              <NavLink href="#/bookings">My Bookings</NavLink>
            </MDBNavItem>
          : ""  }
          </MDBNavbarNav>
          <MDBNavbarNav right>
          {sessionStorage.token ?
            <MDBNavItem>
              <MDBNavLink className="waves-effect waves-light" to="#/" onClick={handleLogout}>Logout
                <MDBIcon fab icon="twitter" />
              </MDBNavLink>
            </MDBNavItem>
            : ""  }
            <MDBNavItem>
              <MDBNavLink className="waves-effect waves-light" to="#!">
                <MDBIcon fab icon="google-plus-g" />
              </MDBNavLink>
            </MDBNavItem>
          </MDBNavbarNav>
        </MDBCollapse>
      </MDBNavbar>
    </Router>
    );
  }
export default Navbar;