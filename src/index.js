import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';


import 'bootstrap/dist/css/bootstrap.min.css';

import "react-toastify/dist/ReactToastify.css";

import 'react-day-picker/lib/style.css'

import Book from "./views/book/Book";

import Bookings from './views/book/Bookings';

import Calendar from "./views/components/Calendar";

import CheckoutForm from "./views/payments/CheckoutForm";

import Payments from "./views/payments/Payments";

import csvData from "./views/csvData/csvData";



ReactDOM.render(<App />, document.getElementById('root'));


serviceWorker.unregister();
