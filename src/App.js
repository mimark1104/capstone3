import React from 'react';

import {HashRouter, Route, Switch} from 'react-router-dom';

const loading = () => {
  return (
    <div className="bg-warning d-flex justify-content-center align-items-center">
        <h1 className="neon">Loading...</h1>
    </div>
  )
} 
// activated when calling the component
const Book = React.lazy(() => import ('./views/book/Book'));

const Bookings = React.lazy(() => import ('./views/book/Bookings'));

const Payments = React.lazy(() => import ('./views/payments/Payments'));

const Register = React.lazy(()=>import('./views/pages/Register'));

const Login = React.lazy(()=>import('./views/pages/Login'));

const Landing = React.lazy(()=>import('./views/landing/Landing'));

const App = () => {
  return (
    <HashRouter>

      <React.Suspense fallback={loading()}>
        <Switch>
        <Route 
            path="/"
            exact
            name="Landing"
            render={props => <Landing {...props} />}
          />
        <Route 
            path="/register"
            exact
            name="Register"
            render={props => <Register {...props} />}
          />
          <Route 
            path="/login"
            exact
            name="Login"
            render={props => <Login {...props}/>}
          />
          <Route
            path="/book"
            exact
            name="Book"
            render={props => <Book {...props} />}
          />
          <Route
            path="/bookings"
            exact
            name="Bookings"
            render={props => <Bookings {...props} />}
          />
          <Route
            path="/payments"
            exact
            name="Payments"
            render={props => <Payments {...props} />}
          />
        </Switch>
      </React.Suspense>
    </HashRouter>
  )
}
export default App;