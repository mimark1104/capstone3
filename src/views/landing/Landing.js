import React, {useEffect, useState} from 'react';
import './style.css';
import {Link} from 'react-router-dom';
import {Button} from 'reactstrap';
import Navbar from '../../Layout/Navbar';
import {
	Lb,
	banana,
    chance,
    couple,
    holo
} from './';

const Landing = () => {

	const [user, setUser] = useState({})

	useEffect(()=>{
		if (sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			setUser(user);
			// window.location.replace("#/book")
		}else{
			console.log(user);
		}
	}, [])
    return(
        <React.Fragment>
        <Navbar/>

<div id="home">
	<div class="container">
		{/* <div class="row"> */}
			{/* <div class="col-md-5 col-sm-3"></div>
			<div class="col-md-7 col-sm-9"> */}
			{!sessionStorage.token ?
            <Link to="/login">
            <button
            className="btn-landing btn-info"
            >Click to Book</button>
            </Link>
			: "" }
			{/* </div> */}
		{/* </div> */}
	</div>
</div>

<div class="divider">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6">
				<div class="divider-wrapper divider-one">
					<img src={couple}/>
					<h2 className="card-title2">LOVE</h2>
					<p>is more of a feeling that a person feels for another person. People often confuse love and lust. Love means to be deeply committed and connected to someone or something.</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="divider-wrapper divider-two">
				<img src={banana}/>
					<h2 className="box2">LIVE</h2>
					<p>When live rhymes with give, it's a verb that has to do with existing being or staying alive, or making your home in a particular place, like when you say, "I live on the planet Earth for the time being."</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-12">
				<div class="divider-wrapper divider-three">
				<img src={chance}/>
					<h2 className="box3">SURVIVE</h2>
					<p>Survivalist ethics depended on identifying 'fittest' with morally best, an improper association since 'fittest', biologically, meant only those who successfully adapted to conditions.</p>
				</div>
			</div>
		</div>
	</div>
</div>
{/*this is event page  */}
<div class="container2">
<h6 class="victory"><span class="victory-v">A</span>wesome</h6>
      <div class="row align-items-center">
        <div class="col-lg-6 order-lg-2">
          <div class="p-5">
            <img class="img-fluid" src="http://www.phawker.com/wp-content/uploads/2010/08/mgmt_520.gif" alt=""/>
          </div>
        </div>
        <div class="col-lg-6 order-lg-1">
          <div class="p-5">
            <h2 class="display-4">For those about to rock...</h2>
            <p>Stand up and be counted
				For what you are about to receive
				We are the dealers
				We'll give you everything you need
				Hail hail to the good times
				'Cause rock has got the right of way
				We ain't no legend, ain't no 'cause
				We're just livin' for today.</p>
          </div>
        </div>
      </div>
    </div>
	<div class="container2">
      <div class="row align-items-center">
        <div class="col-lg-6">
          <div class="p-5">
            <img class="img-fluid" src="https://hiphop-n-more.com/wp-content/uploads/2019/06/chance-the-rapper-new-album-tracklist.jpg" alt=""/>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="p-5">
            <h2 class="display-4">We salute you!</h2>
            <p>We rock at dawn on the front line
				Like a bolt right out of the blue
				The sky's a-light with the guitar bite
				Heads will roll and rock tonight.</p>
          </div>
        </div>
      </div>
    </div>
	<div class="container2">
      <div class="row align-items-center">
        <div class="col-lg-6 order-lg-2">
          <div class="p-5">
            <img class="img-fluid" src="https://stillsound.files.wordpress.com/2011/04/beastie-boys-sydney.jpeg" alt=""/>
          </div>
        </div>
        <div class="col-lg-6 order-lg-1">
          <div class="p-5">
            <h2 class="display-4">Let there be music for Everyone!</h2>
            <p>Let it shine like the sun
				Let there be music
				Everybody's got to have some fun

				It's so hard to keep on smillin'
				When people try to make you frown
				But, we're all in this together
				So why run one another down

				You know we can't expect our problems
				To grow wings and fly away
				So fight the fight just as hard as you can
				When it's time for you to play.</p>
          </div>
        </div>
      </div>
    </div>
	{/* end */}
<div id="about">
	<div className="container">
		<div className="row">
			<div className="col-md-6 col-sm-12">
				<img src={Lb} className="img-responsive my-5 astro" alt="about img"/>
			</div>
			<div className="col-md-6 col-sm-12 about-des">
				<h7 className="neon">Camp Now</h7>
				<p>We are HOLO, A Hostel on the go.  .</p>
				<p>HOLO Camp is located in San Juan, La Union. It boasts trendy interiors, and various exhibitions and other interesting events are held here from time to time.</p>
				<br/>
				<p>explore our list of Camp sites</p>
				{!sessionStorage.token ?
            <Link to="/login">
            <button
            className="btn btn-info"
            >Click to Book</button>
            </Link>
			: "" }
			</div>
		</div>
	</div>
</div>


         
        
        </React.Fragment>
    )
}
export default Landing;