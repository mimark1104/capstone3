import Lb from "./images/landing1.gif";
import banana from "./images/banana.gif";
import chance from "./images/chance.gif";
import couple from "./images/couple.gif";
import holo from "./images/Astro2.jpeg";

export{
    Lb,
    banana,
    chance,
    couple,
    holo
}