import React from 'react';
import {Button} from 'reactstrap';

const BookingRow = (props) => {
    const booking = props.booking;
    return (
        <React.Fragment>
            <tr>
                <td>{booking.firstName}</td>
                <td>{booking.lastName}</td>
                <td>{booking.email}</td>
                <td>{booking.campType}</td>
                <td>{booking.checkIn}</td>
                <td>{booking.checkOut}</td>
                <td>{booking.totalFees}</td>
                <td>

                <Button
                    className="btn-cancel"
                    color="danger"
                    onClick={() => props.handleDeleteBooking(booking._id)}>CANCEL BOOKING</Button>
                
                </td>
            </tr>
        </React.Fragment>
    )
}
export default BookingRow;