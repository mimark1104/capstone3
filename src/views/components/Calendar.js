import React from 'react';
import moment from 'moment';
import Helmet from 'react-helmet';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

import { formatDate, parseDate } from 'react-day-picker/moment';

export default class Calendar extends React.Component {
    constructor(props) {
        super(props);
        this.handleFromChange = this.props.handleFromChange.bind(this);
        this.handleToChange = this.props.handleToChange.bind(this);
        this.state = {
            checkIn: undefined,
            checkOut: undefined,
        };
    }

    showFromMonth() {
        const { checkIn, checkOut } = this.state;
            if (!checkIn) {
            return;
        }
            if (moment(checkOut).diff(moment(checkIn), 'months') < 2) {
            this.checkOut.getDayPicker().showMonth(checkIn);
        }
    }

    render() {
        const { checkIn, checkOut } = this.state;
        const modifiers = { start: checkIn, end: checkOut };
        return (        
        <div className="InputFromTo text-info">
            <DayPickerInput className="text-center"
                value={checkIn}
                placeholder="From"
                format="MMMM Do YYYY"
                formatDate={formatDate}
                parseDate={parseDate}
                dayPickerProps={{
                    selectedDays: [checkIn, { checkIn, checkOut }],
                    disabledDays: { before: new Date()},
                    toMonth: checkOut,
                    modifiers,
                    numberOfMonths: 2,
                    onDayClick: () => this.checkOut.getInput().focus(),
                    }}
                onDayChange={this.handleFromChange}
            />{' '} — {' '}
        <span className="InputFromTo-to">
            <DayPickerInput className="text-center bg-info text-danger"
                ref={el => (this.checkOut = el)}
                value={checkOut}
                placeholder="To"
                format="MMMM Do YYYY"
                formatDate={formatDate}
                parseDate={parseDate}
                dayPickerProps={{
                selectedDays: [checkIn, { checkIn, checkOut }],
                disabledDays: { before: new Date()},
                modifiers,
                month: checkOut,
                fromMonth: checkIn,
                numberOfMonths: 2,
                    }}
                onDayChange={this.handleToChange}
            />
        </span>

    <Helmet>
        <style>{`
        .InputFromTo .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
            background-color: black !important;
            color: #4a90e2;
        }
        .InputFromTo .DayPicker-Day {
            border-radius: 0 !important;
        }
        .InputFromTo .DayPicker-Day--start {
            border-top-left-radius: 50% !important;
            border-bottom-left-radius: 50% !important;
        }
        .InputFromTo .DayPicker-Day--end {
            border-top-right-radius: 50% !important;
            border-bottom-right-radius: 50% !important;
        }
        .InputFromTo .DayPickerInput-Overlay {
            width: 550px;
        }
        .InputFromTo-to .DayPickerInput-Overlay {
            margin-left: -198px;
        }
        `}
        </style>
    </Helmet>
        </div>
        );
    }
}