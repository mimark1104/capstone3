import React, {useState} from 'react';
import './book.css';
import Navbar from '../../Layout/Navbar'
import {
    BookNow,
    Love,
    AV,
    Th,
    Neon,
    Selene,
    Girl1,
    Dance1,
    Jedi
} from "./";

const Card = (props) => {
    const [showBookForm, setShowBookForm] = useState(false);

    
    const handleShowBookForm = () => {
        if(!sessionStorage.token){
            window.location.replace('#/login')
        }else{
        setShowBookForm(!showBookForm)
        }
    }

    return(
        <React.Fragment>
            <BookNow
                showBookForm = {showBookForm}
                handleShowBookForm = {handleShowBookForm}

                handleCheckFees = {props.handleCheckFees}
                checkFeesRequired = {props.checkFeesRequired}

                firstName = {props.firstName}
                firstNameRequired = {props.firstNameRequired}
                handleFirstNameChange = {props.handleFirstNameChange}

                lastName = {props.lastName}
                lastNameRequired = {props.lastNameRequired}
                handleLastNameChange = {props.handleLastNameChange}

                email = {props.email}
                emailRequired = {props.emailRequired}
                handleEmailChange = {props.handleEmailChange}

                handleCampType ={props.handleCampType}
                campType = {props.campType}
                handleCampTypeChange = {props.handleCampTypeChange}

                handleFromChange = {props.handleFromChange}
                handleToChange={props.handleToChange}

                allFees = {props.allFees}
                allDays = {props.allDays}

                handleBookingSave = {props.handleBookingSave}
            />
            {/* <body className="background"> */}
            <div>
            <div className="text-center my-5">
                <div className="neon">BOOK</div>
                <div className="flux">NOW</div>
            </div>
            </div>
            <div className="card col-lg-10 offset-xl-1 text-center py-5 my-5 border-0">
                <div className="d-flex">
                    <div className="card-body col-lg-10 py-2 mx-5 my-2">
                        <div>
                            <h3 className="card-title">Aesthetic Voyagers</h3>
                            <img src={AV} alt="" height="300px" width="400px" />
                        </div>
                        <div className="card-footer py-5 my-2">
                        <p>A Perfect Aesthetic Spot for Aesthetic People. If you're a fan of Rad 80's Vibes, This camp is the best place for you.</p>
                        <ul>
                            <h4 className="outlaw">Arcade Machines</h4>
                            <h4 className="outlaw">Lofi & Vaporwave</h4>
                            <h4 className="outlaw">______________</h4>
                            <h5>Php 3000</h5>
                        </ul>
                        <blockquote className="blockquote">
                            <p className="mb-0">"This village is One of the best!"</p>
                            <footer className="blockquote-footer">Facebook CEO,
                                <cite title="Source Title"> Mark Zuckerberg</cite>
                            </footer>
                        </blockquote>
                            <button
                                className="btn btn-danger btn-block"
                                onClick={handleShowBookForm}
                                >BOOK NOW
                            </button>
                        </div>
                    </div>

                    <div className="card-body col-lg-10 py-2 mx-5 my-2">
                        <div>
                            <h3 className="card-title">Tree Huggers Ville</h3>
                            <img src={Th} alt="" height="300px" width="400px" />
                        </div>
                        <div className="card-footer py-5 my-2">
                        <p>Hey Hey Hey Yippieyow-yippiyeah! If you love nature this camp is the best pick for you.</p>
                        <ul>
                            <h4 className="outlaw">Cosmological Nature vibes</h4>
                            <h4 className="outlaw">Psychedelic Rock</h4>
                            <h4 className="outlaw">______________</h4>
                            <h5>Php 3,789</h5>
                        </ul>     
                        <blockquote className="blockquote">
                            <p className="mb-0">"This village is One of the best!"</p>
                            <footer className="blockquote-footer">Facebook CEO,
                                <cite title="Source Title"> Mark Zuckerberg</cite>
                            </footer>
                        </blockquote>
                            <button
                                className="btn btn-danger btn-block"
                                onClick={handleShowBookForm}
                                >BOOK NOW
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="card col-lg-10 offset-xl-1 text-center py-5 my-5 border-0">
                <div className="d-flex">
                    <div className="card-body col-lg-10 py-2 mx-5 my-2">
                        <div>
                            <h3 className="card-title">Neon Camp</h3>
                            <img src={Neon} alt="" height="300px" width="400px" />
                        </div>
                        <div className="card-footer py-5 my-2">
                        <p>A Perfect Aesthetic Spot for Aesthetic People. If you're a fan of Rad 80's Vibes, This camp is the best place for you.</p>
                        <ul>
                            <h4 className="outlaw">Arcade Machines</h4>
                            <h4 className="outlaw">Lofi & Vaporwave</h4>
                            <h4 className="outlaw">______________</h4>
                            <h5>Php 5,000</h5>
                        </ul>
                        <blockquote className="blockquote">
                            <p className="mb-0">"This village is One of the best!"</p>
                            <footer className="blockquote-footer">Facebook CEO,
                                <cite title="Source Title"> Mark Zuckerberg</cite>
                            </footer>
                        </blockquote>
                            <button
                                className="btn btn-danger btn-block"
                                onClick={handleShowBookForm}
                                >BOOK NOW
                            </button>
                        </div>
                    </div>

                    <div className="card-body col-lg-10 py-2 mx-5 my-2">
                        <div>
                            <h3 className="card-title">Selene</h3>
                            <img src={Selene} alt="" height="300px" width="400px" />
                        </div>
                        <div className="card-footer py-5 my-2">
                        <p>The greatest magicians have something to learn
                            From Mister Mistoffelees Conjuring Turn.
                            And you'll all say 
                            Oh! well I never was there ever a cat so clever as Magical Mister Mistoffelees.</p>
                        <ul>
                            <h4 className="outlaw">Cosmological Nature vibes</h4>
                            <h4 className="outlaw">Psychedelic Rock</h4>
                            <h4 className="outlaw">______________</h4>
                            <h5>Php 5,999</h5>
                        </ul>     
                        <blockquote className="blockquote">
                            <p className="mb-0">"This village is One of the best!"</p>
                            <footer className="blockquote-footer">Facebook CEO,
                                <cite title="Source Title"> Mark Zuckerberg</cite>
                            </footer>
                        </blockquote>
                            <button
                                className="btn btn-danger btn-block"
                                onClick={handleShowBookForm}
                                >BOOK NOW
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {/* </body> */}
            
        </React.Fragment>
    )
}
export default Card;