import Card from './Card';
// images in card
import Love from "./images/neon1.gif";
import Girl1 from "./images/girl1.gif";
import Dance1 from "./images/dance1.gif";
import Jedi from "./images/Jedi.gif";
import AV from "./images/Av.jpeg";
import Neon from "./images/Neon.jpg";
import Selene from "./images/selene.jpg";
import Th from "./images/Th.jpg";



import BookNow from "./BookNow";
// fonts
import Font1 from "./fonts/true-crimes.ttf";

export {
    Card,
    BookNow,
    Love,
    AV,
    Th,
    Neon,
    Selene,
    Girl1,
    Dance1,
    Jedi
    
}