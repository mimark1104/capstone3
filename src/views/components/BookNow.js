import React, {useState} from 'react';
import './book.css';
import {
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Button,
    Label,
    FormGroup,
    Modal,
    ModalHeader,
    ModalBody,
    Input

} from 'reactstrap';
import { FormInput } from '../../globalcomponents';
import Calendar from './Calendar';
import Payments from '../payments/Payments';

const BookNow = (props) => {

    const [dropdownOpen, setDropdownOpen] = useState(false);
    const handleCampType = props.handleCampType;
    const toggle = () => setDropdownOpen(!dropdownOpen);

    return(
        <React.Fragment>            
        <Modal
            isOpen={props.showBookForm}
            toggle={props.handleShowBookForm}
            backdrop={false}>
            <ModalHeader
                toggle={props.handleShowBookForm}
                style={{"backgroundColor" : "#731963"}}>
                Choose a campsite
            </ModalHeader>
            <ModalBody
                style={{"backgroundColor" : "#D17A22", "color": "white"}}>
                <div className = "text-center">
                    <div className="neon">
                    <Label>Camp Now</Label>
                    </div>
                    <Dropdown
                        isOpen={dropdownOpen}
                        toggle={toggle}
                        onClick={(e)=> props.handleCampTypeChange()}>
                        <DropdownToggle caret>{props.campType}
                        </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem
                                onClick={() => handleCampType("Aesthetic Voyager")}>
                                Aesthetic Voyager
                            </DropdownItem>
                            <DropdownItem
                                onClick={() => handleCampType("Tree Huggers Ville")}>
                                Tree Huggers Ville
                            </DropdownItem>
                            <DropdownItem
                                onClick={() => handleCampType("Neon Camp")}>
                                Neon Camp
                            </DropdownItem>
                            <DropdownItem
                                onClick={() => handleCampType("Selene")}>
                                Selene
                            </DropdownItem>
                        </DropdownMenu>
                    </Dropdown>
                </div>
                <FormInput
                    label={"First Name"}
                    type={"text"}
                    name={"firstName"}
                    required={props.firstNameRequired}
                    onChange={props.handleFirstNameChange}
                />
                <FormInput
                    label={"Surname"}
                    type={"text"}
                    name={"lastName"}
                    required={props.lastNameRequired}
                    onChange={props.handleLastNameChange}
                />
                <FormInput
                    label={"Email Address"}
                    type={"email"}
                    name={"email"}
                    required={props.emailRequired}
                    onChange={props.handleEmailChange}
                />
                <Label>Booking Date</Label>
                <Calendar
                    handleFromChange = {props.handleFromChange}
                    handleToChange = {props.handleToChange}
                />
                <div className="py-2">
                </div>
                
                <div className="py-2"></div>

                <div className="d-flex text-center">
                    <FormGroup className="col-lg-6">
                        <Label>Total No. of Days</Label>
                        <Input
                            value = {props.allDays}
                            readOnly/>
                    </FormGroup>
                    <FormGroup className="col-lg-6">
                        <Label>Total Fees(EXCL. of Taxes)</Label>
                        <Input
                            value = {props.allFees}
                            readOnly />
                    </FormGroup>
                </div>

                <Button                    
                    block
                    color="danger"
                    onClick={props.handleCheckFees}
                    disabled = {
                        props.handleFromChange === "" &&
                        props.handleToChange === "" ? true : false
                        }
                        >CHECK FEES
                </Button>
                <Button                    
                    block
                    color="info"
                    onClick = {props.handleBookingSave}
                    disabled = {
                            props.firstName !== "" &&
                            props.lastName !== "" &&
                            props.email !== "" &&
                            props.campType !==  "--- Plese Select A Camp Type ---" &&
                            props.handleFromChange !== "" &&
                            props.handleToChange !== "" ? false : true
                            }
                            >BOOK NOW
                </Button>
                <Payments
                />
            </ModalBody>
        </Modal>
        
    </React.Fragment>
    )
}
export default BookNow;