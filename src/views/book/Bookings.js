import React, {useEffect, useState} from 'react';
import BookingRow from '../components/BookingRow';
import axios from 'axios';
import './bookings.css';
import Navbar from '../../Layout/Navbar'
import CsvDownload from 'react-json-to-csv';

const Bookings = () => {
    const [booking, setBooking] = useState([]);
    const [user, setUser] = useState({});

    useEffect(()=>{
  
        // authentication
        if(sessionStorage.token){
            let user = JSON.parse(sessionStorage.user);
            if(user.isAdmin) {
                // CAME FROM BACKEND book_router
                fetch('https://whispering-plains-62952.herokuapp.com/showbook/')
                .then(response => response.json())
                // SAVE THE DATA IN STATE
                .then(data => {setBooking(data);
                })
                setUser(user)
            } else {
                axios.get('https://whispering-plains-62952.herokuapp.com/showbookbyuser/' + user.id).then(res=>{
                setBooking(res.data)
                })
            }
            setUser(user);
        } else {
            window.location.replace("#/login")
        }
    }, [])

    
    const handleDeleteBooking = (bookingId) => {
        // access to the endpoint
        axios({
            method : "DELETE",
            url: "https://whispering-plains-62952.herokuapp.com/deletebookbyid/" + bookingId
        }).then (res => {
            let newBooking = booking.filter(booking => booking._id != bookingId);
            setBooking(newBooking)
        });
    }
    
    return(
        <React.Fragment>
            <Navbar />
            <h1 className="text-center py-5 neon">BOOKINGS</h1>
            {user.isAdmin ? <CsvDownload className="btn btn-primary" data={booking} />:""}
            <table className="table table-striped col-lg-10 offset-lg-1">
                    <thead>
                        <tr className="text-center">
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Camp Type</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                            <th>Total Fees</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {booking.map (booking =>
                            <BookingRow
                                key = { booking ._id }
                                booking = { booking }
                                handleDeleteBooking = {handleDeleteBooking}
                            />
                            )}
                    </tbody>
                </table>
        </React.Fragment>
    )
}
export default Bookings;