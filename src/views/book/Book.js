import React, {useState} from 'react';
import axios from 'axios';
import moment from 'moment';
import Card from '../components/Card';
import Navbar from '../../Layout/Navbar'
import '../components/book.css';

const Book = () => {
    const [booking, setBooking] = useState([]);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [campType, setCampType] = useState("--- Plese Select A Camp ---");
    const [checkIn, setCheckIn] = useState("");
    const [checkOut, setCheckOut] = useState("");
    
    const [firstNameRequired, setFirstNameRequired] = useState(true);
    const [lastNameRequired, setLastNameRequired] = useState(true);
    const [emailRequired, setEmailRequired] = useState(true);

    const [allFees, setAllFees] = useState();
    const [allDays, setAllDays] = useState();

    const handleBookingSave = async (props) => {
        let checkInDate = moment(checkIn).format("MMMM Do YYYY")
        let checkOutDate = moment(checkOut).format("MMMM Do YYYY")
        let campClass = (campType);
        let totalFees = (allFees);
        let user = JSON.parse(sessionStorage.user)
        sessionStorage.setItem('SessAllFees', totalFees);

        axios.post('https://whispering-plains-62952.herokuapp.com/addbook', {
            userId: user.id,
            firstName : firstName,
            lastName : lastName,
            email : email,
            campType : campClass,
            checkIn : checkInDate,
            checkOut : checkOutDate,
            totalFees : totalFees
        }).then (res => {
            let newBooking = [...booking];
            newBooking.push(res.data);
            setBooking(newBooking);
        })
    }
    const handleCheckFees = () => {
        let difference = new Date(checkOut).getTime() - new Date(checkIn).getTime(); 
        let totalDays = Math.floor(difference / (1000 * 60 * 60 * 24));

        let totalFees = 0;
        let amount = 0;
        
        if (campType === "Aesthetic Voyager") {
            amount = 3000;
            totalFees = amount * totalDays;
        } else if (campType === "Tree Huggers Ville") {
            amount = 3789;
            totalFees = amount * totalDays;
        } else if (campType === "Neon Camp") {
            amount = 5000;
            totalFees = amount * totalDays;
        } else if (campType === "Selene") {
            amount = 5999;
            totalFees = amount * totalDays;
        }

        setAllFees(totalFees)
        setAllDays(totalDays)
    }

    const handleFirstNameChange = (e) => {
        if (e.target.value === "") {
            setFirstNameRequired(true);
            setFirstName("");
        }else{
            setFirstNameRequired(false);
            setFirstName(e.target.value);
        }
    }

    const handleLastNameChange = (e) => {
        if (e.target.value === "") {
            setLastNameRequired(true);
            setLastName("");
        }else{
            setLastNameRequired(false);
            setLastName(e.target.value);
        }
    }

    const handleEmailChange = (e) => {
        if (e.target.value === "") {
            setEmailRequired(true);
            setEmail("");
        }else{
            setEmailRequired(false);
            setEmail(e.target.value);
        }
    }

    const handleCampTypeChange = (e) => {
    }


    const handleFromChange = (checkIn) => {
        setCheckIn(checkIn);
    }
    const handleToChange = (checkOut) => {
        setCheckOut(checkOut);
    }

    const handleCampType = (campType) => {
        // console.log(campType)
        setCampType(campType)
    }

    return(
        <React.Fragment>
            <Navbar />
            {/* <div className="jumbotron jumbotron-fluid background">
            <div className="container text-center">
                <div className="neon">BOOK</div>
                <div className="flux">NOW</div>
            </div>
            </div> */}
            <div>
            <Card
                handleCheckFees = {handleCheckFees}

                firstName = {firstName}
                firstNameRequired = {firstNameRequired}
                handleFirstNameChange = {handleFirstNameChange}

                lastName = {lastName}
                lastNameRequired = {lastNameRequired}
                handleLastNameChange = {handleLastNameChange}

                email = {email}
                emailRequired = {emailRequired}
                handleEmailChange = {handleEmailChange}

                campType = {campType}
                handleCampType = {handleCampType}
                handleCampTypeChange = {handleCampTypeChange}

                handleFromChange = {handleFromChange}
                handleToChange={handleToChange}

                allFees = {allFees}
                allDays = {allDays}

                handleBookingSave = {handleBookingSave}
            />
            </div>
        </React.Fragment>
    )
}
export default Book;