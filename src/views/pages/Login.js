import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {FormInput} from '../../globalcomponents'
import {Button} from 'reactstrap'
import axios from 'axios'
import Navbar from '../../Layout/Navbar'
import '../components/book.css';

const Login = () => {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [user, setUser] = useState({})

	useEffect(()=>{
		if (sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			setUser(user);
			window.location.replace("#/book")
		}else{
			console.log(user);
		}
	}, [])

	const handleEmailChange = e => {
		setEmail(e.target.value);
	}

	const handlePasswordChange = e => {
		setPassword(e.target.value);
	}

	const handleLogin = () => {
		axios.post('https://whispering-plains-62952.herokuapp.com/login',{
			email,
			password
		}).then(res=>{
			sessionStorage.token = res.data.token;
			sessionStorage.user = JSON.stringify(res.data.user)
			console.log(res.data.token)

			window.location.replace('#/book')
		})
	}

	const handleToRegister =() =>{
		window.location.replace('#/register')
	}

	return (
		<React.Fragment>
			<h1 className="text-center py-5 neon">Login</h1>
			<div
				className="col-lg-4 offset-lg-4"
			>
			<FormInput 
				label={"Email"}
				placeholder={"Enter your email"}
				type={"email"}
				onChange={handleEmailChange}
			/>
			<FormInput 
				label={"Password"}
				placeholder={"Enter your password"}
				type={"password"}
				onChange={handlePasswordChange}
			/>
			<Button
				className="btn btn-info"
				block
				color="info"
				onClick={handleLogin}
			>
				Login
			</Button>
			<p>Not Signed Up yet?</p>
			<Button
				className="btn btn-danger"
				block
				color="danger"
				onClick={handleToRegister}
			>
				Register
			</Button>
			</div>
		</React.Fragment>
		)
}
export default Login;