import React, {useState, useEffect} from 'react';
import {FormInput} from '../../globalcomponents';
import {
	Button
} from 'reactstrap';
import '../components/book.css';
import axios from 'axios';


const Register = () => {


	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [user, setUser] = useState({})

	useEffect(()=>{
		if (sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			setUser(user);
			window.location.replace("#/book")
		}else{
			console.log(user);
		}
	}, [])

	const handleNameChange = (e) => {
		setName(e.target.value)
		console.log(e.target.value)
	}

	const handleEmailChange = (e) => {
		setEmail(e.target.value);
		console.log(e.target.value)
	}

	const handlePasswordChange = e => {
		setPassword(e.target.value);
		console.log(e.target.value);
	}

	const handleRegister = () => {
		axios.post("https://whispering-plains-62952.herokuapp.com/register", {
			name: name,
			email: email,
			password: password
		}).then(res=>console.log(res.data))
		window.location.replace('#/login');
	}

	return (

		<React.Fragment>
			<h1 className="text-center py-5 neon">Register</h1>
			<div className="col-lg-4 offset-lg-4">
				<FormInput 
					label={"Name"}
					placeholder={"Enter your name"}
					type={"text"}
					onChange={handleNameChange}
				/>
				<FormInput 
					label={"Email"}
					placeholder={"Enter your email"}
					type={"email"}
					onChange={handleEmailChange}
				/>
				<FormInput 
					label={"Password"}
					placeholder={"Enter your password"}
					type={"password"}
					onChange={handlePasswordChange}
				/>
				<Button
					className="btn btn-danger"
					block
					color="info"
					onClick={handleRegister}
				>
					Register
				</Button>
			</div>			
		</React.Fragment>
	)
}
export default Register;