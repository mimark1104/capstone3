import React, { useState } from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import {Button} from 'reactstrap';
import axios from 'axios';

const CheckoutForm = (props) => {

	const submit =  async (e) => {
		let totalAmt = sessionStorage.getItem('SessAllFees') * 100;
		axios.post('https://whispering-plains-62952.herokuapp.com/charge', {
			amount: totalAmt
		}).then(res => {
			console.log("success")
			sessionStorage.removeItem('SessAllFees')
		})
		window.location.replace("#/bookings")
	}

	return (		
		<div className="checkout col-lg-8 text-center">
        	<CardElement />			
            <Button
				className="py-2"
				color="success"
				onClick={submit}
				disabled = {
					sessionStorage.getItem('SessAllFees') <= 0 ? true : false
				}
				>Book now & pay
			</Button>
      	</div>
	)
}
export default injectStripe(CheckoutForm);



