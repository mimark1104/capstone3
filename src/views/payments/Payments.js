import React from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import StripeScriptLoader from 'react-stripe-script-loader'
import CheckoutForm from './CheckoutForm';

const Payments = () => {
	return (
		<React.Fragment>
			<StripeScriptLoader
				uniqueId='myUniqueId'
				script='https://js.stripe.com/v3/'
				loader="Loading..."
				>
				<StripeProvider apiKey="pk_test_QjQzYh2cXFhOhcsSEBd2qQGQ00ldoHdeOe">
						<div className="d-flex justify-content-center py-2">
							<Elements>
								<CheckoutForm />
							</Elements>
						</div>
				</StripeProvider>
			</StripeScriptLoader>
		</React.Fragment>
	)
}
export default Payments;